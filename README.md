# jenkins-dind

Docker image which allows for running Docker in Docker (DinD). `kubectl` is also included to run on Kubernetes clusters creating an end-to-end CI/CD toolchain capable of deploying to the cluster it is running on.

## Requirements

* The container must be run with `--privileged` in order for nested-Docker to work. If running in Kubernetes, using the `runAsUser: 0` securityContext is required. 